# Comandos MongoDB #

Exemplos de alguns comandos do MongoDB

### Criando coleção ###

```
#!javascript
use loja
```

### Adicionando documentos ###

```
#!javascript
var prod1 = {"name" : "TV", "price" : "15"};
var prod2 = {"name" : "Geladeira", "price" : "13"};
var prod3 = {"name" : "Celular", "price" : "8"};
```

```
#!javascript
db.produtos.insert(prod1);
db.produtos.insert(prod2);
db.produtos.insert(prod3);
```

### Removendo documentos ###



### Busca com valores especificos ###
```
#!javascript
db.produtos.find({"price":"8"})
```

### Query selectors ###

$or: Retorna na busca todos os dados que tenham pelo menos um atributo da busca.
```
#!javascript
db.produtos.find({
  "$or": [
    {
      "name" : "TV"
    },
    {
      "price" : "8"
    }
  ]
})
```

### Retornar só as chaves que precisamos, em vez do objeto inteiro ###
#!javascript
db.produtos.find({}, {"name" : 1})
```